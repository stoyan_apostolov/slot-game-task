class Player {

  private double balance;
  private double bet;
  private double winAmount;

  Player(double balance) {
    this.balance = balance;
  }

  void hasWonOrLost(boolean winCondition1, boolean winCondition2) {

    if (winCondition1 || winCondition2) {
      this.winAmount = this.bet * 2;
      this.balance += this.winAmount;
    } else {
      this.winAmount = 0;
    }
  }

  void placeBet(double bet) {
    this.bet = bet;
    this.balance -= bet;
  }

  double getBalance() {
    return balance;
  }

  double getWinAmount() {
    return winAmount;
  }
}

