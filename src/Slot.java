import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;
import java.util.Random;

class Slot {

  private int randomize(int range) {

    Random random = new Random();
    return random.nextInt(range);
  }

  List<String> readFileLines() {

    try {
      return Files.readAllLines(Paths.get("C:\\JavaWorkspace\\GanTask\\src\\reels_template.txt"));
    } catch (IOException e) {
      e.printStackTrace();
      return null;
    }
  }

  String[][] fillMatrix(List<String> allLines) {

    String[][] matrix = new String[3][5];
    int elementCounter = 1;
    int lineNumber;

    for (int col = 0; col < matrix[0].length; col++) { //matrix[0:2].length = 5

      if (col == 1 || col == 3) {

        lineNumber = randomize(allLines.size() - 5);//skipping the NA symbols

      } else {

        lineNumber = randomize(allLines.size() - 3);//in worst case scenario 3 lines before the last one
      }
      for (int row = 0; row < matrix.length; row++) {  //matrix.length = 3

        String[] currentLine = allLines.get(lineNumber + row).split("\\s+");
        matrix[row][col] = currentLine[elementCounter];

      }
      elementCounter++;
    }

    return matrix;
  }

  boolean threeInARow(String[][] matrix) {

    int counter = 1;

    for (int row = 0; row < matrix.length; row++) {

      counter = 1;

      for (int col = 0; col < matrix[row].length - 1; col++) {

        if (matrix[row][col].equals(matrix[row][col + 1])) {
          counter++;
        } else {
          counter = 1;
        }

        if (counter == 3) {
          return true;
        }
      }
    }
    return false;
  }

  boolean threeInDiagonal(String[][] matrix) {

    boolean cond1 = matrix[0][0].equals(matrix[1][1]);
    boolean cond2 = matrix[1][1].equals(matrix[2][2]);
    boolean cond3 = matrix[2][2].equals(matrix[1][3]);
    boolean cond4 = matrix[1][3].equals(matrix[0][4]);

    return cond1 && cond2 || cond3 && cond4;
  }
}
