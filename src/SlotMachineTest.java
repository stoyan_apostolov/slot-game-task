import org.junit.Assert;
import org.junit.Test;

public class SlotMachineTest {

  @Test
  public void win_condition_for_three_in_a_row_is_met1() {

    //GIVEN
    Slot slot = new Slot();
    String[][] matrix = {{"Q", "K", "K", "K", "A"},
            {"J", "A", "K", "S1", "A"},
            {"J", "S1", "J", "A", "K"}};

    //WHEN
    boolean conditionIsMet = slot.threeInARow(matrix);

    //THEN
    Assert.assertTrue(conditionIsMet);
  }

  @Test
  public void win_condition_for_three_in_a_row_is_met2() {

    //GIVEN
    Slot slot = new Slot();
    String[][] matrix = {{"Q", "Q", "K", "K", "K"},
            {"J", "A", "K", "S1", "A"},
            {"J", "S1", "J", "A", "K"}};

    //WHEN
    boolean conditionIsMet = slot.threeInARow(matrix);

    //THEN
    Assert.assertTrue(conditionIsMet);
  }

  @Test
  public void win_condition_for_three_in_a_row_is_met3() {

    //GIVEN
    Slot slot = new Slot();
    String[][] matrix = {{"Q", "K", "J", "K", "A"},
            {"S1", "S1", "S1", "S1", "A"},
            {"J", "S1", "J", "A", "K"}};

    //WHEN
    boolean conditionIsMet = slot.threeInARow(matrix);

    //THEN
    Assert.assertTrue(conditionIsMet);
  }

  @Test
  public void win_condition_for_three_in_a_row_is_met4() {

    //GIVEN
    Slot slot = new Slot();
    String[][] matrix = {{"Q", "K", "A", "K", "A"},
            {"J", "A", "K", "S1", "A"},
            {"J", "J", "J", "J", "J"}};

    //WHEN
    boolean conditionIsMet = slot.threeInARow(matrix);

    //THEN
    Assert.assertTrue(conditionIsMet);
  }

  @Test
  public void win_condition_for_three_in_a_row_is_met5() {

    //GIVEN
    Slot slot = new Slot();
    String[][] matrix = {{"S2", "S2", "S1", "Q", "Q"},
            {"S2", "S2", "S1", "S1", "Q"},
            {"S2", "S2", "S1", "S1", "Q"}};

    //WHEN
    boolean conditionIsMet = slot.threeInARow(matrix);

    //THEN
    Assert.assertFalse(conditionIsMet);
  }

  @Test
  public void win_condition_for_three_in_diagonal_is_met1() {

    //GIVEN
    Slot slot = new Slot();
    String[][] matrix = {{"Q", "J", "K", "S1", "A"},
            {"J", "Q", "K", "S1", "A"},
            {"J", "S1", "Q", "A", "K"}};

    //WHEN
    boolean conditionIsMet = slot.threeInDiagonal(matrix);

    //THEN
    Assert.assertTrue(conditionIsMet);
  }

  @Test
  public void win_condition_for_three_in_diagonal_is_met2() {

    //GIVEN
    Slot slot = new Slot();
    String[][] matrix = {{"A", "J", "K", "S1", "Q"},
            {"J", "S1", "K", "Q", "A"},
            {"J", "S1", "Q", "A", "K"}};

    //WHEN
    boolean conditionIsMet = slot.threeInDiagonal(matrix);

    //THEN
    Assert.assertTrue(conditionIsMet);
  }
}
