import java.util.Scanner;

public class Game {

  public static void main(String[] args) {

    Slot slot = new Slot();
    Player player = new Player(10000);
    Scanner scanner = new Scanner(System.in);
    boolean wantsToPlay = true;

    while (wantsToPlay) {

      System.out.printf("Balance %.1f\n", player.getBalance());
      System.out.println("Place bet and spin or type exit to leave: ");

      String line = scanner.nextLine();

      if (line.equals("exit")) {

        wantsToPlay = false;

      } else if (line.matches("\\d+(\\.\\d+)?(\\d+)?")) {

        double currentBetAmount = Double.parseDouble(line);

        if (player.getBalance() > 0 && player.getBalance() > currentBetAmount) {

          player.placeBet(currentBetAmount);

        } else {
          System.out.println("Insufficient balance! Game over.");
          return;
        }

        System.out.println("Outcome: ");

        String[][] matrix = slot.fillMatrix(slot.readFileLines());

        for (String[] strings : matrix) {
          System.out.println();
          for (String string : strings) {
            System.out.print(string + " ");
          }
        }

        System.out.println();
        System.out.println();

        player.hasWonOrLost(slot.threeInARow(matrix), slot.threeInDiagonal(matrix));

        System.out.printf("Balance: %.1f\n", player.getBalance());
        System.out.printf("Win: %.1f\n", player.getWinAmount());

      } else {
        System.out.println("Invalid command. Place bet and spin or type exit to leave: ");
      }
    }
  }
}
